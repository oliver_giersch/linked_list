cmake_minimum_required(VERSION 3.10)
project(linked_list LANGUAGES CXX)

set(INC_DIR ${PROJECT_SOURCE_DIR}/include)
set(SRC_DIR ${PROJECT_SOURCE_DIR}/src)

add_executable(main ${SRC_DIR}/main.cpp)